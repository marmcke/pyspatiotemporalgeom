
Source Code
================

Package source code is hosted at bitbucket:

https://bitbucket.org/marmcke/pyspatiotemporalgeom/



Introduction
=============


On open source geometry processing library focusing on regions and moving regions.

Algorithms for constructing regions, identifying connected components, creating moving regions, and performing operations on moving regions are included.

Some 3D operations are included, such as triangle/triangle intersection and ray/triangle intersection.

In general, a regions is represented as a list of segments; a moving region is represented as a list of triangles.  


This software is open source and distributed under the MIT license.





See documentation for details at http://www.cs.siue.edu/~marmcke/docs/pyspatiotemporalgeom/.

The package is hosted at PyPi!

Installing
==============

To install, download the package, unpack, change the working directory to the top level directory of the unpacked directory, and use ``python setup.py install``.

Or use ``easy_install pyspatiotemporalgeom``.

Or use the preferred method: ``pip install pyspatiotemporalgeom``.


Contributors
=================

+ Mark McKenney  http://www.cs.siue.edu/~marmcke/index.html
+ Niharika Nyalakonda
+ Jarrod McEvers
+ Mitchell Shipton
+ Josh Winchester


