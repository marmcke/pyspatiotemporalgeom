import sys
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
fig = plt.figure()
ax = Axes3D(fig)


if len( sys.argv ) >= 2:
        fileName = sys.argv[1]
else:
        print( 'Enter a filename')
        exit()

file = open(fileName, 'r' )

bx1 = []
by1 = []
bz1 = []
bx2 = []
by2 = []
bz2 = []

ex1 = []
ey1 = []
ez1 = []
ex2 = []
ey2 = []
ez2 = []

mx1 = []
my1 = []
mz1 = []
mx2 = []
my2 = []
mz2 = []

which = 0
pois = []
for line in file:
        if line == 'E\n' or line == 'M\n':
                which+=1
                continue
        if line == '\n':
                which+=1
                continue
        if len(line)>0 and line[0] =='#':
                continue
        pois.append( line )
        if len(pois) == 3:
            numbers1 = pois[0].split()
            numbers2 = pois[1].split()
            numbers3 = pois[2].split()
            pois = []
            if which == 0:
                    bx1.append( float( numbers1[0] ) )
                    by1.append(  float( numbers1[1] ) )
                    bz1.append(  float( numbers1[2] ) )
                    bx2.append(  float( numbers2[0] ) )
                    by2.append(  float( numbers2[1] ) )
                    bz2.append(  float( numbers2[2] ) )
                    bx1.append( float( numbers1[0] ) )
                    by1.append(  float( numbers1[1] ) )
                    bz1.append(  float( numbers1[2] ) )
                    bx2.append(  float( numbers3[0] ) )
                    by2.append(  float( numbers3[1] ) )
                    bz2.append(  float( numbers3[2] ) )
                    bx1.append( float( numbers2[0] ) )
                    by1.append(  float( numbers2[1] ) )
                    bz1.append(  float( numbers2[2] ) )
                    bx2.append(  float( numbers3[0] ) )
                    by2.append(  float( numbers3[1] ) )
                    bz2.append(  float( numbers3[2] ) )
            elif which == 1:
                    ex1.append( float( numbers1[0] ) )
                    ey1.append(  float( numbers1[1] ) )
                    ez1.append(  float( numbers1[2] ) )
                    ex2.append(  float( numbers2[0] ) )
                    ey2.append(  float( numbers2[1] ) )
                    ez2.append(  float( numbers2[2] ) )
                    ex1.append( float( numbers1[0] ) )
                    ey1.append(  float( numbers1[1] ) )
                    ez1.append(  float( numbers1[2] ) )
                    ex2.append(  float( numbers3[0] ) )
                    ey2.append(  float( numbers3[1] ) )
                    ez2.append(  float( numbers3[2] ) )
                    ex1.append( float( numbers2[0] ) )
                    ey1.append(  float( numbers2[1] ) )
                    ez1.append(  float( numbers2[2] ) )
                    ex2.append(  float( numbers3[0] ) )
                    ey2.append(  float( numbers3[1] ) )
                    ez2.append(  float( numbers3[2] ) )
            else:
                    mx1.append( float( numbers1[0] ) )
                    my1.append(  float( numbers1[1] ) )
                    mz1.append(  float( numbers1[2] ) )
                    mx2.append(  float( numbers2[0] ) )
                    my2.append(  float( numbers2[1] ) )
                    mz2.append(  float( numbers2[2] ) )
                    mx1.append( float( numbers1[0] ) )
                    my1.append(  float( numbers1[1] ) )
                    mz1.append(  float( numbers1[2] ) )
                    mx2.append(  float( numbers3[0] ) )
                    my2.append(  float( numbers3[1] ) )
                    mz2.append(  float( numbers3[2] ) )
                    mx1.append( float( numbers2[0] ) )
                    my1.append(  float( numbers2[1] ) )
                    mz1.append(  float( numbers2[2] ) )
                    mx2.append(  float( numbers3[0] ) )
                    my2.append(  float( numbers3[1] ) )
                    mz2.append(  float( numbers3[2] ) )
                
for i in range( len(bx1) ):
        ax.plot( [bx1[i], bx2[i]], [by1[i], by2[i]], [bz1[i], bz2[i]], color=(1,0,0),linewidth=1.2 );
        if i % 500 == 0:
                print( i )
                
for i in range( len(ex1) ):
        ax.plot( [ex1[i], ex2[i]], [ey1[i], ey2[i]], [ez1[i], ez2[i]], color=(0,1,0), linewidth=1.2 );
        if i % 500 == 0:
                print (i)
                
for i in range( len(mx1) ):
        ax.plot( [mx1[i], mx2[i]], [my1[i], my2[i]], [mz1[i], mz2[i]], color=(0,0,1), linewidth=.9 );
        if i % 500 == 0:
                print (i)
                
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Time')

plt.show()
        
                
                
                
                
